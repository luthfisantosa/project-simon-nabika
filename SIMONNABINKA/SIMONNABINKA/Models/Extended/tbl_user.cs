﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMONNABINKA.Models
{
    [MetadataType(typeof(tbl_user_MetaData))]
    public partial class tbl_user
    {
        public string login_error_message { get; set; }
        public HttpPostedFileBase imageFile { get; set; }
        public IList<ms_role> ms_Roles { get; set; }
        public IList<ms_stat_user> ms_stat_users { get; set; }

        //extended
    }

    public class tbl_user_MetaData
    {
        [DataType(DataType.Password)]
        public string password { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> birthDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string address { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_created_user { get; set; }
    }
}