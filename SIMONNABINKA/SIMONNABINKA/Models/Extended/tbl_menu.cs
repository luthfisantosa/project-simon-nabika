﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMONNABINKA.Models
{
    [MetadataType(typeof(tbl_menu_MetaData))]
    public partial class tbl_menu
    {
        public HttpPostedFileBase imageMenu { get; set; }
        public string opration { get; set; }
        [Range(0, 99)]
        public int inStock { get; set; }
    }

    public class tbl_menu_MetaData
    {
        public int PK_id_menu { get; set; }
        public int FK_id_type { get; set; }
        public string itemName { get; set; }
        [DataType(DataType.Currency)]
        public Nullable<int> price { get; set; }
        public string menu_photo { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_created_menu { get; set; }
        public Nullable<int> stock { get; set; }
    }

}