﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMONNABINKA.Models
{
    [MetadataType(typeof(tr_headTransaction_MetaData))]
    public partial class tr_headTransaction
    {
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> minDate { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> maxDate { get; set; }
    }

    public class tr_headTransaction_MetaData
    {
        public int PK_id_transaction { get; set; }
        public string FK_id_user { get; set; }
        public int FK_id_transactionStat { get; set; }
        public int FK_id_voucher { get; set; }
        public string buyerName { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> purchaseDate { get; set; }
        [DataType(DataType.Currency)]
        public Nullable<int> countPrice { get; set; }
    }
}