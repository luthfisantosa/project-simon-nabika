﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMONNABINKA.Models
{
    [MetadataType(typeof(tbl_voucher_MetaData))]
    public partial class tbl_voucher
    {
        public string login_error_message { get; set; }

        //extended
    }

    public class tbl_voucher_MetaData
    {
        public int PK_id_voucher { get; set; }
        public string nameVoucher { get; set; }
        public Nullable<int> discountVoucher { get; set; }
        public string FK_id_user { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> expiredDate { get; set; }
        public Nullable<int> totalVoucher { get; set; }
        public Nullable<int> usedVoucher { get; set; }
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_created_voucher { get; set; }
        public string created_by { get; set; }
    }
}