﻿using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SIMONNABINKA.Controllers
{
    public class AuthController : Controller
    {
        loginContext db = new loginContext();

        // GET: Auth
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Message = "Login";
            return View();
        }

        [HttpPost]
        public ActionResult Authorize(SIMONNABINKA.Models.tbl_user userModel)
        {
            try
            {
                //Define Hash String
                //ENCRYPT PASSWORD
                userModel.password = Encrypt(userModel.password);

                var userDetails = db.tbl_user.Where(x => x.username == userModel.username && x.password == userModel.password).FirstOrDefault();
                //DECRYPT PASS INTO STRING
                userDetails.password = Decrypt(userDetails.password);
                if (userDetails.FK_id_userStat == "ACT")
                {
                    Session["userID"] = userDetails.username;
                    Session["Role"] = userDetails.FK_id_role;
                    Session["avatar"] = userDetails.userPhoto;
                    Session["password"] = userDetails.password;
                    return RedirectToAction("Transaction", "Home");
                }
                else
                {
                    userModel.login_error_message = "Your account is suspended, please report to super admin to reactive!";
                    ViewBag.notif = userModel.login_error_message;
                }
            }
            catch (Exception ex)
            {
                userModel.login_error_message = "Wrong username or password!";
                ViewBag.notif = userModel.login_error_message;
                ViewBag.exception = ex;
            }

            userModel.password = Decrypt(userModel.password);
            return View("index", userModel);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Auth");
        }

        //encrypt function
        public string Encrypt(string encryptString)   
        {  
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);  
            using(Aes encryptor = Aes.Create())   
            {  
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {  
                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76  
                });  
                encryptor.Key = pdb.GetBytes(32);  
                encryptor.IV = pdb.GetBytes(16);  
                using(MemoryStream ms = new MemoryStream())  
                {  
                    using(CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {  
                        cs.Write(clearBytes, 0, clearBytes.Length);  
                        cs.Close();  
                    }  
                    encryptString = Convert.ToBase64String(ms.ToArray());  
                }  
            }  
            return encryptString;  
        }

        //decrypt function
        public string Decrypt(string cipherText)   
        {  
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
            cipherText = cipherText.Replace(" ", "+");  
            byte[] cipherBytes = Convert.FromBase64String(cipherText);  
            using(Aes encryptor = Aes.Create())   
            {  
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {  
                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76  
                });  
                encryptor.Key = pdb.GetBytes(32);  
                encryptor.IV = pdb.GetBytes(16);  
                using(MemoryStream ms = new MemoryStream())   
                {  
                    using(CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)) {  
                        cs.Write(cipherBytes, 0, cipherBytes.Length);  
                        cs.Close();  
                    }  
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());  
                }  
            }  
            return cipherText;  
        }  

    }
}