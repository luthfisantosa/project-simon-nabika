﻿using SIMONNABINKA.Models;
using System;
using System.Data;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;


namespace SIMONNABINKA.Controllers
{
    public class ItemController : Controller
    {
        loginContext db = new loginContext();

        // GET: Item
        public ActionResult Index()
        {
            return View();
        }
        

        [HttpGet]
        public ActionResult EditMenu()
        {
            ViewBag.header = "Edit Menu";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.EditMenu = "active";
            ViewBag.FK_id_type = new SelectList(db.ms_menu_type.Where(x => x.status == "active"), "PK_id_type", "menu_type");


            return View();
        }

        [HttpPost]
        public ActionResult EditMenu(tbl_menu newMenu)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {

                    if (ModelState.IsValid)
                    {
                        newMenu.date_created_menu = DateTime.Today;
                        db.tbl_menu.Add(newMenu);
                        db.SaveChanges();
                        int lastMenuId = db.tbl_menu.Max(item => item.PK_id_menu);
                        var changeMenu = db.tbl_menu.Where(s => s.PK_id_menu == lastMenuId).First();
                        string name = Path.GetFileNameWithoutExtension(changeMenu.menu_photo);
                        var ext = Path.GetExtension(changeMenu.menu_photo);
                        string myfile = name + "_" + lastMenuId.ToString() + ext;
                        string path = Path.Combine(Server.MapPath("~/Content/Picture/Menu Picture/"), myfile);
                        changeMenu.menu_photo = myfile;
                        newMenu.imageMenu.SaveAs(path);
                        db.SaveChanges();
                        tr.Commit();

                    }

                }
                catch (Exception ex)
                {

                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();

                }

                return RedirectToAction("CrudMenu");

            }

        }



        [HttpGet]
        public ActionResult EditCategory()
        {
            ViewBag.header = "Edit Category";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.EditCategory = "active";

            List<ms_menu_type> updateListCategory = db.ms_menu_type.ToList();
            TempData["updateListCategory"] = updateListCategory;
            return View();

        }

        [HttpPost]
        public ActionResult EditCategory(ms_menu_type categoryIn)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {

                    if (ModelState.IsValid)
                    {
                        db.ms_menu_type.Add(categoryIn);
                        db.SaveChanges();

                        tr.Commit();
                        return RedirectToAction("EditCategory");
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();
                }
                
            }

            return RedirectToAction("EditCategory");
        }

        [HttpGet]
        public ActionResult CrudMenu()
        {
            if (TempData["shortMessage"] != null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }
            ViewBag.header = "Menu List";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.EditMenu = "active";
            ViewBag.FK_id_type = new SelectList(db.ms_menu_type, "PK_id_type", "menu_type");

            List<tbl_menu> updateListMenu = db.tbl_menu.ToList();
            TempData["updateListMenu"] = updateListMenu;
            return View();

        }

        [HttpPost]
        public ActionResult CrudMenu(tbl_menu menuIn)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {
                    var updateMenu = db.tbl_menu.Find(menuIn.PK_id_menu);

                    if (ModelState.IsValid)
                    {

                        if (TryUpdateModel(updateMenu))
                        {

                            updateMenu.FK_id_type = menuIn.FK_id_type;
                            updateMenu.itemName = menuIn.itemName;
                            updateMenu.price = menuIn.price;
                            db.SaveChanges();

                            tr.Commit();
                            TempData["shortMessage"] = "success";
                        }
                        return RedirectToAction("CrudMenu");
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();
                }
            }

            return RedirectToAction("CrudMenu");
        }

        [HttpPost]
        public ActionResult DeleteMenu(tbl_menu id)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {
                    var deleteMenu = db.tbl_menu.Where(s => s.PK_id_menu == id.PK_id_menu).First();

                    if (TryUpdateModel(deleteMenu))
                    {
                        db.tbl_menu.Remove(deleteMenu);
                        db.SaveChanges();
                        tr.Commit();
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();
                }
            }
            return RedirectToAction("CrudMenu");
        }


        //disabled
        [HttpPost]
        public ActionResult Delete(ms_menu_type id)
        {
            var deleteCategory = db.ms_menu_type.Where(s => s.PK_id_type == id.PK_id_type ).First();
            
            if (TryUpdateModel(deleteCategory))
            {
                db.ms_menu_type.Remove(deleteCategory);
                db.SaveChanges();

            }
            return RedirectToAction("EditCategory");
        }


        [HttpPost]
        public ActionResult Edit(ms_menu_type idIn)
        {
            var updateStatus = db.ms_menu_type.Find(idIn.PK_id_type);
            if (TryUpdateModel(updateStatus))
            {
                updateStatus.status = idIn.status;
                updateStatus.menu_type = idIn.menu_type;
                db.SaveChanges();
            }
            return RedirectToAction("EditCategory");
        }

        [HttpGet]
        public ActionResult Stock()
        {
            if (TempData["shortMessage"] != null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }
            ViewBag.header = "Stock";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.Stock = "active";
            List<tbl_menu> updateListStock = db.tbl_menu.ToList();
            TempData["updateListStock"] = updateListStock;

            return View();
        }

        [HttpPost]
        public ActionResult Stock(tbl_menu stockIn)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var editStock = db.tbl_menu.Where(s => s.PK_id_menu == stockIn.PK_id_menu).First();
                    if (stockIn.opration == "Increase ( + )")
                    {
                        TempData["shortMessage"] = "success";
                        editStock.stock = editStock.stock + stockIn.inStock;
                        editStock.date_created_menu = DateTime.UtcNow;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    else
                    {
                        TempData["shortMessage"] = "success";
                        editStock.stock = editStock.stock - stockIn.inStock;
                        editStock.date_created_menu = stockIn.date_created_menu;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    
                }
                catch (Exception ex)
                {
                    ViewBag.notif = "page stock : " + ex.Message;
                    transaction.Rollback();
                }
                return RedirectToAction("Stock");
            }
        }
        [HttpPost]
        public ActionResult ResetStock(tbl_menu resetIn)
        {
            var editStock = db.tbl_menu.Where(s => s.PK_id_menu == resetIn.PK_id_menu).First();
            if (TryUpdateModel(editStock))
            {
                TempData["shortMessage"] = "success";
                editStock.stock = 0;
                editStock.date_created_menu = resetIn.date_created_menu;
                db.SaveChanges();

            }
           
            return RedirectToAction("Stock");
        }
        
        [HttpGet]
        public ActionResult ListVoucher()
        {
            ViewBag.header = "List Voucher";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ListVoucher = "active";
            return View();
        }
    }
}