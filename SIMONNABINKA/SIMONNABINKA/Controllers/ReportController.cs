﻿using Microsoft.Reporting.WebForms;
using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SIMONNABINKA.Controllers
{
    [AllowAnonymous]
    public class ReportController : Controller
    {
        loginContext db = new loginContext();

        // GET: Report
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ReportStocks()
        {
            ViewBag.header = "Report Stock";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

            ViewBag.ReportStocks = "active";
            //List<tbl_menu> updateListStock = db.tbl_menu.ToList();
            //TempData["updateListStock"] = updateListStock;

            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1000);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/simonNabinka_stockReport";

            ViewBag.ReportViewer = report;

            return View();
        }

        [HttpGet]
        public ActionResult ReportTransaction()
        {
            ViewBag.header = "Report Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ReportTransaction = "active";
            var session = Session["Role"].ToString();
            if (session == "A")
            {
                string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report = new ReportViewer();
                report.ProcessingMode = ProcessingMode.Remote;
                report.Width = Unit.Pixel(1000);
                report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
                report.ServerReport.ReportPath = "/simonNabinka_transactionReport_A";

                ViewBag.ReportViewer = report;
            }
            else if (session == "SA")
            {
                string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report = new ReportViewer();
                report.ProcessingMode = ProcessingMode.Remote;
                report.Width = Unit.Pixel(1000);
                report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
                report.ServerReport.ReportPath = "/simonNabinka_transactionReport_SA";

                ViewBag.ReportViewer = report;
            }
            return View();
        }

        [HttpPost]
        public ActionResult ReportTransaction(tr_headTransaction transactionModel)
        {
            var minDate = transactionModel.minDate;
            var maxDate = transactionModel.maxDate;
            ViewBag.header = "Report Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ReportTransaction = "active";
            var session = Session["Role"].ToString();
            if (session == "A")
            {
                List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.Where(x => x.purchaseDate == DateTime.UtcNow).ToList();
                TempData["updateListTransaction"] = updateListTransaction;
            }
            else if (session == "SA")
            {
                if (minDate ==null && maxDate == null)
                {
                    List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.ToList();
                    TempData["updateListTransaction"] = updateListTransaction;
                }
                else if (minDate!=null && maxDate != null)
                {
                    List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.Where(x => x.purchaseDate >= minDate && x.purchaseDate <= maxDate).ToList();
                    TempData["updateListTransaction"] = updateListTransaction;
                }else if (minDate == null)
                {
                    List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.Where(x => x.purchaseDate <= maxDate).ToList();
                    TempData["updateListTransaction"] = updateListTransaction;
                }else if (maxDate == null)
                {
                    List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.Where(x => x.purchaseDate <= maxDate).ToList();
                    TempData["updateListTransaction"] = updateListTransaction;
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult DetailReportTransaction()
        {
            ViewBag.header = "Detail Report Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ReportTransaction = "active";
            var session = Session["Role"].ToString();
            if (session == "A")
            {
                //DetailsView trans
                string ssrsUrl2 = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report2 = new ReportViewer();
                report2.ProcessingMode = ProcessingMode.Remote;
                report2.Width = Unit.Pixel(1000);
                report2.ServerReport.ReportServerUrl = new Uri(ssrsUrl2);
                report2.ServerReport.ReportPath = "/simonNabinka_detailTransactionReport";

                ViewBag.DetailReportViewer = report2;
            }
            else if (session == "SA")
            {
                //DetailsView trans
                string ssrsUrl2 = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
                ReportViewer report2 = new ReportViewer();
                report2.ProcessingMode = ProcessingMode.Remote;
                report2.Width = Unit.Pixel(1000);
                report2.ServerReport.ReportServerUrl = new Uri(ssrsUrl2);
                report2.ServerReport.ReportPath = "/simonNabinka_detailTransactionReport";

                ViewBag.DetailReportViewer = report2;
            }
            return View();
        }

        [HttpGet]
        public ActionResult ReportLogError()
        {
            ViewBag.header = "Report Log Error";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ReportLogError = "active";

            //List<tbl_logError> updateListError= db.tbl_logError.ToList();
            //TempData["updateListError"] = updateListError;

            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1000);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/simonNabinka_logErrorReport";

            ViewBag.ReportViewer = report;
            return View();
        }

        [HttpGet]
        public ActionResult ReportUsedVoucher()
        {
            ViewBag.header = "Report Used Voucher";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ReportUsedVoucher = "active";

            //List<tr_headTransaction> updateListTransaction = db.tr_headTransaction.Where(x=>x.tbl_voucher.usedVoucher > 0).ToList();
            //TempData["updateListTransaction"] = updateListTransaction;

            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1000);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/simonNabinka_usedVoucherReport";
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}