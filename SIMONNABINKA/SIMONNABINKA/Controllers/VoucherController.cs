﻿using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIMONNABINKA.Controllers
{
    public class VoucherController : Controller
    {
        loginContext db = new loginContext();
        // GET: Voucher
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult VoucherView()
        {
            if (TempData["shortMessage"] != null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }

            ViewBag.header = "Voucher List";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.ListVoucher = "active"; 

            List<tbl_voucher> updateListVoucher = db.tbl_voucher.Where(x=>x.nameVoucher!="VOID").ToList();
            TempData["updateListVoucher"] = updateListVoucher;
            return View();

        }

        [HttpGet]
        public ActionResult CreateVoucher()
        {
            if (TempData["shortMessage"] != null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }

            return View();
        }

        [HttpPost]
        public ActionResult CreateVoucher(SIMONNABINKA.Models.tbl_voucher voucherModel)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    //cek duplicate voucher name
                    var count = db.tbl_voucher.Where(x => x.nameVoucher == voucherModel.nameVoucher).Count();
                    if (count > 0)
                    {
                        TempData["shortMessage"] = "dupplicate";
                        return RedirectToAction("CreateVoucher");
                    }
                    
                    var session = Session["userID"].ToString();
                    var getUser = db.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    voucherModel.FK_id_user = getUser.PK_id_user;
                    voucherModel.date_created_voucher = DateTime.UtcNow;
                    voucherModel.created_by = getUser.name;
                    voucherModel.usedVoucher = 0;
                    db.tbl_voucher.Add(voucherModel);
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["shortMessage"] = "success";
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page voucher : " + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page voucher [" + controllerName + "] :" + ex.Message;


                    transaction.Rollback();
                }
                return RedirectToAction("VoucherView");
            }
        }

        [HttpPost]
        public ActionResult VoucherView(tbl_voucher voucherIn)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var updateVoucher = db.tbl_voucher.Where(s => s.PK_id_voucher == voucherIn.PK_id_voucher).First();
                    var date = updateVoucher.expiredDate;

                    updateVoucher.nameVoucher = voucherIn.nameVoucher;
                    updateVoucher.discountVoucher = voucherIn.discountVoucher;
                    updateVoucher.totalVoucher = voucherIn.totalVoucher;
                    updateVoucher.status = voucherIn.status;
                    //modified to last edited
                    updateVoucher.date_created_voucher = DateTime.UtcNow;
                    if (voucherIn.expiredDate.HasValue)
                    {
                        updateVoucher.expiredDate = voucherIn.expiredDate;
                    }
                    else
                    {
                        updateVoucher.expiredDate = date;
                    }

                    db.SaveChanges();
                    transaction.Commit();
                    TempData["shortMessage"] = "success";
                }
                catch (Exception ex)
                {
                    ViewBag.exception = ex;
                    transaction.Rollback();
                }

                return RedirectToAction("VoucherView");
            }
        }

        [HttpPost]
        public ActionResult DeleteVoucher(tbl_voucher id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var deleteVoucher = db.tbl_voucher.Where(s => s.PK_id_voucher == id.PK_id_voucher).First();

                    if (TryUpdateModel(deleteVoucher))
                    {
                        db.tbl_voucher.Remove(deleteVoucher);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page voucher : " + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page voucher [" + controllerName + "] :" + ex.Message;

                    transaction.Rollback();
                }
            }
            
            return RedirectToAction("VoucherView");
        }




    }
}