﻿using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace SIMONNABINKA.Controllers
{
    public class DashboardController : Controller
    {
        loginContext db = new loginContext();

        // GET: Dashboard
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.header = "Dashboard";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.dashboard = "active";
            
            List<tbl_menu> updateListStock = db.tbl_menu.ToList();
            TempData["updateListStock"] = updateListStock;
            return View();
        }
        
        [WebMethod]
        public JsonResult GetData()
        {
            var data = db.tbl_menu.Where(x=>x.FK_id_type==3).Sum(x=>x.stock).ToString();
            if(data == "0" || data == null) data = "0";
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult GetDrink()
        {
            var data = db.tbl_menu.Where(x => x.FK_id_type == 2).Sum(x => x.stock).ToString();
            if(data == "0" || data == null) data = "0";
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}