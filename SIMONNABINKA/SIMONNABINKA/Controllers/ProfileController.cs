﻿using SIMONNABINKA.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace SIMONNABINKA.Controllers
{
    public class ProfileController : Controller
    {
        loginContext db = new loginContext();
        // GET: Profile
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.header = "Profile";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

           
            var session = Session["userID"].ToString();
            ViewBag.Profile = "active";
            return View(db.tbl_user.Where(x => x.username == session).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult Profile()
        {
            if (TempData["shortMessage"]!=null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }
            ViewBag.header = "Profile";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.Profile = "active";

            var session = Session["userID"].ToString();
            return View(db.tbl_user.Where(x => x.username == session).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult ChangePhoto(SIMONNABINKA.Models.tbl_user userModel)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    userModel.PK_id_user.ToString();
                    string filePath = Server.MapPath("~/Content/Picture/Profile Picture/") + Path.GetFileName(userModel.imageFile.FileName);

                    userModel.username = userModel.username;
                    userModel.name = userModel.name;
                    userModel.FK_id_role = userModel.FK_id_role;
                    userModel.password = userModel.password;
                    userModel.birthDate = userModel.birthDate;
                    userModel.gender = userModel.gender;
                    userModel.FK_id_userStat = userModel.FK_id_userStat;
                    userModel.date_created_user = userModel.date_created_user;
                    userModel.created_by = userModel.created_by;
                    userModel.address = userModel.address;
                    userModel.phoneNumber = userModel.phoneNumber;

                    userModel.imageFile.SaveAs(filePath);
                    userModel.userPhoto = Path.GetFileName(userModel.imageFile.FileName);

                    Session["avatar"] = userModel.userPhoto;
                    db.Entry(userModel).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["shortMessage"] = "success";

                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page profile : [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page profile : " + ex.Message;

                    transaction.Rollback();
                    TempData["shortMessage"] = "Page Profile: " + ex.Message;
                }
                return RedirectToAction("Profile");
            }
        }

        [HttpPost]
        public ActionResult Profile(SIMONNABINKA.Models.tbl_user userModel)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    userModel.PK_id_user.ToString();
                    userModel.username = userModel.username;
                    userModel.FK_id_role = userModel.FK_id_role;
                    userModel.password = userModel.password;
                    userModel.birthDate = userModel.birthDate;
                    userModel.gender = userModel.gender;
                    userModel.FK_id_userStat = userModel.FK_id_userStat;
                    userModel.date_created_user = userModel.date_created_user;
                    userModel.created_by = userModel.created_by;
                    userModel.userPhoto = userModel.userPhoto;
                    
                    db.Entry(userModel).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["shortMessage"] = "success";
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page profile : [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page profile : "+ ex.Message;

                    transaction.Rollback();
                    TempData["shortMessage"] = "Page Profile: " + ex.Message;
                }
                return RedirectToAction("Profile");
            }            
        }

        

        [HttpPost]
        public ActionResult ChangePassword(SIMONNABINKA.Models.tbl_user userModel, string old_password, string new_password, string re_password)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var session = Session["userID"].ToString();
                    var userDetail = db.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    if (userDetail.password == old_password)
                    {
                        if (userDetail.password == new_password)
                        {
                            //return Content($"Ops, old and new password can't be the same! {userDetail.password}");
                            TempData["shortMessage"] = "duplicate_old";
                        }
                        else
                        {
                            userModel.PK_id_user.ToString();
                            userModel.username = userModel.username;
                            userModel.name = userModel.name;
                            userModel.FK_id_role = userModel.FK_id_role;
                            userModel.birthDate = userModel.birthDate;
                            userModel.gender = userModel.gender;
                            userModel.FK_id_userStat = userModel.FK_id_userStat;
                            userModel.date_created_user = userModel.date_created_user;
                            userModel.created_by = userModel.created_by;
                            userModel.address = userModel.address;
                            userModel.phoneNumber = userModel.phoneNumber;
                            userModel.userPhoto = userModel.userPhoto;

                            string pass = new_password;
                            userModel.password = pass;

                            //Define Hash String
                            //ENCRYPT PASSWORD
                            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
                            byte[] clearBytes = Encoding.Unicode.GetBytes(userModel.password);  
                            using(Aes encryptor = Aes.Create())   
                            {  
                                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {  
                                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76  
                                });  
                                encryptor.Key = pdb.GetBytes(32);  
                                encryptor.IV = pdb.GetBytes(16);  
                                using(MemoryStream ms = new MemoryStream())  
                                {  
                                    using(CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)) {  
                                        cs.Write(clearBytes, 0, clearBytes.Length);  
                                        cs.Close();  
                                    }
                                    userModel.password = Convert.ToBase64String(ms.ToArray());  
                                }  
                            }  

                            db.Entry(userModel).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["shortMessage"] = "success";                            
                           
                        }
                    }
                    else
                    {
                        TempData["shortMessage"] = "old_unmatch";
                        return RedirectToAction("Profile");
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page profile : ["+ controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page profile : " + ex.Message;

                    transaction.Rollback();
                }
            }
            return RedirectToAction("Profile");
        }
        
    }
}