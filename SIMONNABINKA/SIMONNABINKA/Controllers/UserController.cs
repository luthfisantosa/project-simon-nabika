﻿using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace SIMONNABINKA.Controllers
{
    public class UserController : Controller
    {
        loginContext db = new loginContext();


        // GET: User
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Header = "Userlist Owner";
            ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.UserOwner = "active";
            return View();
        }

        [HttpGet]
        public ActionResult UserOwner()
        {
            ViewBag.Header = "Userlist Owner";
            ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.UserOwner = "active";
        

            ViewBag.FK_id_userStat = new SelectList(db.ms_stat_user, "PK_id_userStat", "userStat");
            List<tbl_user> updateList = db.tbl_user.Where(c => c.FK_id_role == "SA").ToList();
            TempData["updateList"] = updateList;
            return View();

        }

        [HttpPost]
        public ActionResult UpdateStat(tbl_user statusIn)
        {
            var updateStatus = db.tbl_user.Find(statusIn.PK_id_user);
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    
                    if (TryUpdateModel(updateStatus))
                    {
                        updateStatus.FK_id_userStat = statusIn.FK_id_userStat;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                }
                catch(Exception ex)
                {
                    //add error here
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    ViewBag.notif = "page user : "+ex.Message;
                    transaction.Rollback();
                }
            }

            if (updateStatus.FK_id_role == "SA")
            {
                return RedirectToAction("UserOwner");
            }
            else
            {
                return RedirectToAction("UserWaiter");
            }
        }

        [HttpGet]
        public ActionResult createOwner()
        {
            ViewBag.FK_id_role = new SelectList(db.ms_role, "PK_id_role", "roleName");
            ViewBag.FK_id_userStat = new SelectList(db.ms_stat_user, "PK_id_userStat", "userStat");
            return View();
        }

        [HttpPost]
        public ActionResult createOwner(tbl_user userModel)
        {
            var cekid = userModel.FK_id_role;
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var session = Session["userID"];
                    var cekUser = userModel.username;
                    var count = db.tbl_user.Where(s => s.username.Contains(cekUser)).Count();
                    if (count > 0)
                    {
                        ViewBag.message = "Title already exists";
                        return View();
                    }
                    else
                    {
                        userModel.date_created_user = DateTime.UtcNow;
                        userModel.password = Encrypt("123");
                        userModel.userPhoto = "default.png";
                        userModel.created_by = session.ToString();
                        if (cekid == "SA")
                        {
                            //GENERATE AUTO ID
                            var lastId = db.tbl_user.AsNoTracking().Where(x => x.FK_id_role == "SA").Max(x => x.PK_id_user);
                            var code = lastId.Substring(0,3);
                            var getnum = lastId.Substring(3);
                            int num = Convert.ToInt32(getnum);
                            num = num + 1;
                            var id = code + num.ToString();

                            userModel.PK_id_user = id;
                        }
                        else if(cekid == "A")
                        {
                            //GENERATE AUTO ID
                            var lastId = db.tbl_user.AsNoTracking().Where(x => x.FK_id_role == "A").Max(x => x.PK_id_user);
                            var code = lastId.Substring(0, 3);
                            var getnum = lastId.Substring(3);
                            int num = Convert.ToInt32(getnum);
                            num = num + 1;
                            var id = code + num.ToString();

                            userModel.PK_id_user = id;
                        }

                        db.tbl_user.Add(userModel);
                        db.SaveChanges();
                        transaction.Commit();
                        if (cekid == "SA")
                        {
                            return RedirectToAction("UserOwner");
                        }
                        else
                        {
                            return RedirectToAction("UserWaiter");
                        }
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page ["+ controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    transaction.Rollback();
                }
                return RedirectToAction("UserOwner");
            }
        }

        [HttpGet]
        public ActionResult Delete(string id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var delete = db.tbl_user.Where(s => s.PK_id_user == id).First();
                    db.tbl_user.Remove(delete);
                    db.SaveChanges();
                    transaction.Commit();
                    if (delete.FK_id_role == "SA")
                    {
                        return RedirectToAction("UserOwner");
                    }
                    else
                    {
                        return RedirectToAction("UserWaiter");
                    }
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    transaction.Rollback();
                    return RedirectToAction("UserOwner");
                }                
            }
        }
        [HttpGet]
        public ActionResult Detail(string id)
        {

            var detail = db.tbl_user.Where(d => d.PK_id_user == id).First();

            //DECRYPT PASS INTO STRING
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";  
            detail.password = detail.password.Replace(" ", "+");  
            byte[] cipherBytes = Convert.FromBase64String(detail.password);  
            using(Aes encryptor = Aes.Create())   
            {  
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {  
                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76  
                });  
                encryptor.Key = pdb.GetBytes(32);  
                encryptor.IV = pdb.GetBytes(16);  
                using(MemoryStream ms = new MemoryStream())   
                {  
                    using(CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)) {  
                        cs.Write(cipherBytes, 0, cipherBytes.Length);  
                        cs.Close();  
                    }
                    detail.password = Encoding.Unicode.GetString(ms.ToArray());  
                }  
            }

            return View(detail);
        }

        [HttpGet]
        public ActionResult UserWaiter()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    ViewBag.Header = "Userlist Waiter";
                    ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
                    ViewBag.UserWaiter = "active";
                    //return View(db.tbl_user.Where(c => c.FK_id_role == "SA").ToList());

                    ViewBag.FK_id_userStat = new SelectList(db.ms_stat_user, "PK_id_userStat", "userStat");
                    List<tbl_user> updateList = db.tbl_user.Where(c => c.FK_id_role == "A").ToList();
                    TempData["updateList"] = updateList;
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    transaction.Rollback();
                }

                return View();
            }
           
        }

        //encrypt function
        public string Encrypt(string encryptString)
        {
            string EncryptionKey = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
                });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }
    }
}