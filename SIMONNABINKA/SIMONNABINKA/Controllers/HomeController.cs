﻿using SIMONNABINKA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIMONNABINKA.Controllers
{
    public class HomeController : Controller
    {
        loginContext db = new loginContext();

        public ActionResult Index()
        {
            return View();
        }
        
//<<<<<<< HEAD
//        [HttpGet]
//        public ActionResult Transaction()
//        {
//            ViewBag.header = "Transaction";
//            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

//            ViewBag.transaction = "active";
//            ViewBag.role = "admin";
//            ViewBag.user= Session["userID"].ToString();

//            List<tbl_menu> updateListFood = db.tbl_menu.Where(s=>s.ms_menu_type.menu_type == "Food" ).ToList();
//            TempData["updateListFood"] = updateListFood;

//            List<tbl_menu> updateListDrink = db.tbl_menu.Where(s => s.ms_menu_type.menu_type == "Drink").ToList();
//            TempData["updateListDrink"] = updateListDrink;

//            List<tbl_menu> updateListmenu= db.tbl_menu.ToList();
//            TempData["updateListMenu"] = updateListmenu;

//            return View();
//        }
//=======
       
        public void CheckVoucher()
        {
            var changeVoucher = db.tbl_voucher.Where(s => s.totalVoucher <= 0).ToList();
   
            var expiredVoucher = db.tbl_voucher.Where(s => s.expiredDate <= DateTime.Now).ToList();
            for (var i = 0; i < changeVoucher.Count; i++)
            {
                changeVoucher[i].status = "deactive";
                db.SaveChanges();
            }
            for (var i = 0; i < expiredVoucher.Count; i++)
            {
                expiredVoucher[i].status = "deactive";
                db.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult UnpaidTransaction()
        {
            ViewBag.header = "Unpaid Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.UnpaidTransaction = "active";
            ViewBag.ms_stat_transaction = new SelectList(db.ms_stat_transaction, "PK_id_transactionStat", "statName");

            List<tr_headTransaction> updateListTr = db.tr_headTransaction.Where(s => s.FK_id_transactionStat == 2).ToList();
            TempData["updateListTr"] = updateListTr;

            CheckVoucher();
            List<tbl_voucher> updateListVoucher = db.tbl_voucher.Where(s => s.status == "active").ToList();
            TempData["updateListVoucher"] = updateListVoucher;

            return View("UnpaidTransaction");
        }
        [HttpGet]
        public ActionResult DetailTransaction(int Id)
        {

            ViewBag.Header = "Detail Transaction for ID :  "+Id;
            ViewBag.Date = DateTime.Now.ToString("dd/MM/yyyy");
            ViewBag.UnpaidTransaction = "active";
      
            List<tr_detailOrder> updateListOr = db.tr_detailOrder.Where(s => s.FK_id_transaction == Id).ToList();
            TempData["updateListOr"] = updateListOr;
            

            return View();
        }

        [HttpGet]
        public ActionResult Transaction()
        {
            if (TempData["shortMessage"] != null)
            {
                ViewBag.notif = TempData["shortMessage"].ToString();
            }

            ViewBag.header = "Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

            ViewBag.transaction = "active";
            ViewBag.role = "admin";
            ViewBag.user = Session["userID"].ToString();

            List<tbl_menu> updateListFood = db.tbl_menu.Where(s => s.ms_menu_type.menu_type == "Food" && s.status == "active").ToList();
            TempData["updateListFood"] = updateListFood;

            List<tbl_menu> updateListDrink = db.tbl_menu.Where(s => s.ms_menu_type.menu_type == "Drink" && s.status == "active").ToList();
            TempData["updateListDrink"] = updateListDrink;

            CheckVoucher();
            List<tbl_voucher> updateListVoucher = db.tbl_voucher.Where(s => s.status == "active").ToList();
            TempData["updateListVoucher"] = updateListVoucher;

            return View();
        }

        [HttpPost]

        public ActionResult Save(tr_headTransaction newTrIn)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {
                    var session = Session["userID"].ToString();
                    var getUser = db.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    if (newTrIn.FK_id_voucher.HasValue)
                    {
                        var getVoucherID = db.tbl_voucher.Where(x => x.PK_id_voucher == newTrIn.FK_id_voucher).FirstOrDefault();
                        getVoucherID.totalVoucher = getVoucherID.totalVoucher - 1;
                        getVoucherID.usedVoucher = getVoucherID.usedVoucher + 1;
                        db.SaveChanges();
                    }
                    newTrIn.purchaseDate = DateTime.Now;
                    newTrIn.FK_id_user = getUser.PK_id_user;
                    db.tr_headTransaction.Add(newTrIn);
                    db.SaveChanges();
                    int? testID = null;
                    int lastTransId = db.tr_headTransaction.Max(item => item.PK_id_transaction);
                    var changeId = db.tr_detailOrder.Where(s => s.FK_id_transaction == testID).ToList();
                    for (var i = 0;i<changeId.Count;i++)
                    {
                        changeId[i].FK_id_transaction = lastTransId;
                        db.SaveChanges();
                    }
                 
                    tr.Commit();
                    TempData["shortMessage"] = "success";
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();

                }
                return RedirectToAction("Transaction");
            }
        }

        [HttpPost]

        public ActionResult Transaction(List<tr_detailOrder>list )
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {
                   
                    for (int i = 0; i < list.Count; i++)
                        {
                            var updateStock = db.tbl_menu.Find(list[i].FK_id_menu);
                            var newstock = updateStock.stock - list[i].quantity;
                            updateStock.stock = newstock;
                            db.tr_detailOrder.Add(list[i]);
                            db.SaveChanges();
                    }
                    
                            tr.Commit();
                }
                catch (Exception ex)
                {
                //get current controller name
                //input tbl_logError
                string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                loginContext db1 = new loginContext();
                var session = Session["userID"].ToString();
                var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                tbl_logError error = new tbl_logError();
                error.FK_id_user = getUser.PK_id_user;
                error.error_msg = "page [" + controllerName + "]" + ex.Message;
                error.error_date = DateTime.UtcNow;
                db1.tbl_logError.Add(error);
                db1.SaveChanges();
                ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                tr.Rollback();

            }
            return RedirectToAction("Transaction");
            }
        }


        [HttpPost]
        public ActionResult UpdateStat(tr_headTransaction statusIn)
        {
            var updateStatus = db.tr_headTransaction.Where(s => s.PK_id_transaction == statusIn.PK_id_transaction).FirstOrDefault();
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (TryUpdateModel(updateStatus))
                    {
                        if (statusIn.FK_id_voucher.HasValue)
                        {
                            var getVoucherID = db.tbl_voucher.Where(x => x.PK_id_voucher == statusIn.FK_id_voucher).FirstOrDefault();
                            getVoucherID.totalVoucher = getVoucherID.totalVoucher - 1;
                            getVoucherID.usedVoucher = getVoucherID.usedVoucher + 1;
                            db.SaveChanges();
                        }
                        updateStatus.FK_id_transactionStat = 1;
                        updateStatus.countPrice = statusIn.countPrice;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    //add error here
                    ViewBag.notif = "page user : " + ex.Message;
                    var data = db.tbl_logError.FirstOrDefault();
                    data.FK_id_user = Session["userID"].ToString();
                    data.error_msg = ViewBag.notif;
                    data.error_date = DateTime.UtcNow;
                    transaction.Rollback();
                }
            }

            return RedirectToAction("UnpaidTransaction");
       
        }

        [HttpGet]
        public ActionResult TransactionUnpaid(int? id)
        {
            ViewBag.header = "Transaction";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

            ViewBag.transaction = "active";
            ViewBag.role = "admin";
            ViewBag.user = Session["userID"].ToString();

            List<tbl_menu> updateListFood = db.tbl_menu.Where(s => s.ms_menu_type.menu_type == "Food").ToList();
            TempData["updateListFood"] = updateListFood;

            List<tbl_menu> updateListDrink = db.tbl_menu.Where(s => s.ms_menu_type.menu_type == "Drink").ToList();
            TempData["updateListDrink"] = updateListDrink;



            Session["ID"] = id;

            return View();
        }

        [HttpPost]

        public ActionResult TransactionUnpaid(List<tr_detailOrder> list)
        {
            using (var tr = db.Database.BeginTransaction())
            {
                try
                {
                    var newInt = Convert.ToInt32(Session["ID"]);
                    for (int i = 0; i < list.Count; i++)
                    {
                        var updateStock = db.tbl_menu.Find(list[i].FK_id_menu);
                        var newstock = updateStock.stock - list[i].quantity;
                        updateStock.stock = newstock;
                        list[i].FK_id_transaction = newInt;
                        db.tr_detailOrder.Add(list[i]);
                        db.SaveChanges();
                    }
                    var joinStock = db.tr_detailOrder.Where(s => s.FK_id_transaction == newInt).ToList();
                    var updateTrans = db.tr_headTransaction.Where(s => s.PK_id_transaction == newInt).FirstOrDefault();
                    var input = joinStock;
                    var newTotal = 0;  
                    var result = joinStock.GroupBy(item => new { item.FK_id_menu, item.FK_id_transaction })
                                .Select(group => new {
                                    FK_id_transaction = newInt,
                                    group.Key.FK_id_menu,
                                    quantity = Convert.ToInt32(group.Sum(c => c.quantity)),
                                    price = Convert.ToInt32(group.Sum(c => c.price))
                                 }).ToList();
                    for (int i = 0; i < joinStock.Count; i++)
                    {
                        db.tr_detailOrder.Remove(joinStock[i]);
                        db.SaveChanges();
                    }
                    for (int i = 0; i < result.Count; i++)
                    {
                        input[i].FK_id_transaction = result[i].FK_id_transaction;
                        input[i].FK_id_menu = result[i].FK_id_menu;
                        input[i].quantity = result[i].quantity;
                        input[i].price = result[i].price;
                        newTotal += result[i].price;
                        db.tr_detailOrder.Add(input[i]);
                        updateTrans.countPrice = newTotal;
                        db.SaveChanges();
                    }
                    updateTrans.countPrice = newTotal;
                    db.SaveChanges();
                    newTotal = 0;
                    tr.Commit();
                    Session.Remove("ID");
                }
                catch (Exception ex)
                {
                    //get current controller name
                    //input tbl_logError
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    loginContext db1 = new loginContext();
                    var session = Session["userID"].ToString();
                    var getUser = db1.tbl_user.AsNoTracking().Where(x => x.username == session).FirstOrDefault();
                    tbl_logError error = new tbl_logError();
                    error.FK_id_user = getUser.PK_id_user;
                    error.error_msg = "page [" + controllerName + "]" + ex.Message;
                    error.error_date = DateTime.UtcNow;
                    db1.tbl_logError.Add(error);
                    db1.SaveChanges();
                    ViewBag.notif = "Page [" + controllerName + "] :" + ex.Message;

                    tr.Rollback();

                }
                return RedirectToAction("UnpaidTransaction");
            }
        }

            
        public ActionResult About()
        {
            ViewBag.header = "About us";
            ViewBag.date = DateTime.Now.ToString("dd/MM/yyyy");

            ViewBag.about = "active";
            ViewBag.role = "admin";
            ViewBag.user = Session["userID"].ToString();

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Auth");
        }
    }
}