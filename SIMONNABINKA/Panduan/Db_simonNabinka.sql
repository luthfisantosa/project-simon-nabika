CREATE DATABASE Db_simonNabinka
--ms_role----------------------------------------------------------------------------------------------
CREATE TABLE ms_role(
	PK_id_role varchar(11) NOT NULL PRIMARY KEY,
	roleName varchar(20)
)
INSERT INTO ms_role (PK_id_role, roleName) VALUES ('SA','super_admin'),('A','suspend')
SELECT * FROM ms_role


--ms_stat_user----------------------------------------------------------------------------------------------
CREATE TABLE ms_stat_user(
	PK_id_userStat varchar(11) NOT NULL PRIMARY KEY,
	userStat varchar(20)
)
INSERT INTO ms_stat_user (PK_id_userStat, userStat) VALUES ('ACT','active'),('SPN','suspend')
SELECT * FROM ms_stat_user


--tbl_user----------------------------------------------------------------------------------------------
DROP TABLE tbl_user
CREATE TABLE tbl_user (
	PK_id_user varchar(11) NOT NULL PRIMARY KEY,
	username varchar(50),
	name varchar(50),
	FK_id_role varchar(11) NOT NULL FOREIGN KEY REFERENCES ms_role(PK_id_role),
	password varchar(255),
	birthDate date,
	gender varchar(2),
	address varchar(255),
	phoneNumber varchar(20),
	userPhoto varchar(255),
	FK_id_userStat varchar(11) NOT NULL FOREIGN KEY REFERENCES ms_stat_user(PK_id_userStat),
	date_created_user date,
	created_by varchar(50),
	CONSTRAINT Check_id_user UNIQUE (PK_id_user),
	CONSTRAINT Check_username_length CHECK (DATALENGTH([username]) <= 50),
	CONSTRAINT Check_name_length CHECK (DATALENGTH([name]) <= 50),
	CONSTRAINT Check_password_length CHECK (DATALENGTH([password]) <= 255),
	CONSTRAINT Check_address_length CHECK (DATALENGTH([address]) <= 255)
)

INSERT INTO tbl_user 
(PK_id_user, 
username, 
name, 
FK_id_role, 
password, 
birthDate, 
gender, 
address, 
phoneNumber, 
userPhoto, 
FK_id_userStat,
date_created_user,
created_by
) VALUES
('ADM001', 'admin', 'Mike Tompkins', 'A', '123', '1990/01/01', 'M', 'jl.Merdeka, Tangerang', '081398764537', 'default.jpg', 'ACT', '2020/08/08', 'super_admin'),
('SAD001', 'super_admin', 'John Tron', 'SA', '123', '1990/04/04', 'M', 'jl.Jambon 32 Yogyakarta', '081398764537', 'default.jpg', 'ACT', '2020/08/08', 'super_admin')

SELECT * FROM tbl_user

--tbl_voucher----------------------------------------------------------------------------------------------
CREATE TABLE tbl_voucher (
	PK_id_voucher varchar(11) NOT NULL PRIMARY KEY,
	nameVoucher varchar(50),
	discountVoucher int,
	FK_id_user varchar(11) NOT NULL FOREIGN KEY REFERENCES tbl_user(PK_id_user),
	expiredDate date,
	totalVoucher int,
	usedVoucher int,
	date_created_voucher date,
	created_by varchar(50)
)

DROP TABLE tbl_voucher 

INSERT INTO tbl_voucher (PK_id_voucher, nameVoucher, discountVoucher, FK_id_user, expiredDate, totalVoucher, usedVoucher,date_created_voucher, created_by)
VALUES ('DIS08082001', 'DISC50', 50, 'SAD001', '2020/08/12', 2, 0, '2020/08/09', 'super_admin')

SELECT * FROM tbl_voucher

--tbl_logError----------------------------------------------------------------------------------------------
CREATE TABLE tbl_logError(
	PK_id_error int NOT NULL PRIMARY KEY,
	FK_id_user varchar(11) NOT NULL FOREIGN KEY REFERENCES tbl_user(PK_id_user),
	error_msg varchar(255),
	error_date datetime	
)

SELECT * FROM tbl_logError

--ms_menu_type----------------------------------------------------------------------------------------------
CREATE TABLE ms_menu_type(
	PK_id_type int NOT NULL PRIMARY KEY,
	menu_type varchar(20)
)

SELECT * FROM ms_menu_type

--ms_stat_transaction----------------------------------------------------------------------------------------------
CREATE TABLE ms_stat_transaction(
	PK_id_transactionStat int NOT NULL PRIMARY KEY,
	statName varchar(255)
)

SELECT * FROM ms_stat_transaction
	  

DROP TABLE ms_stat_transaction

--tbl_menu----------------------------------------------------------------------------------------------
CREATE TABLE tbl_menu(
	PK_id_menu int NOT NULL PRIMARY KEY,
	FK_id_type int NOT NULL FOREIGN KEY REFERENCES ms_menu_type(PK_id_type),
	itemName varchar(50),
	price int,
	stock int,
	menu_photo varchar(255),
	date_created_menu date
)

SELECT * FROM tbl_menu

DROP TABLE tbl_menu

--tr_detailOrder----------------------------------------------------------------------------------------------
CREATE TABLE tr_detailOrder(
	PK_id_order varchar(11) NOT NULL PRIMARY KEY,
	FK_id_menu int NOT NULL FOREIGN KEY REFERENCES tbl_menu(PK_id_menu),
	quantity int,
	price int
)

SELECT * FROM tr_detailOrder

DROP TABLE tr_detailOrder

--tr_headTransaction----------------------------------------------------------------------------------------------
CREATE TABLE tr_headTransaction(
	PK_id_transaction varchar(11) NOT NULL PRIMARY KEY,
	FK_id_user varchar(11) NOT NULL FOREIGN KEY REFERENCES tbl_user(PK_id_user),
	FK_id_order varchar(11) NOT NULL FOREIGN KEY REFERENCES tr_detailOrder(PK_id_order),
	FK_id_transactionStat int NOT NULL FOREIGN KEY REFERENCES ms_stat_transaction(PK_id_transactionStat),
	FK_id_voucher varchar(11) NOT NULL FOREIGN KEY REFERENCES tbl_voucher(PK_id_voucher),
	buyerName varchar(50),
	purchaseDate datetime,
	countPrice int
)

SELECT * FROM tr_headTransaction

DROP TABLE tr_headTransaction